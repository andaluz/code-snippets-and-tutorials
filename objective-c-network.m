- (void)connection:(NSString*)serviceName forIpAddress:(NSString *)ipAddress 
  forPort:(NSString *)portNo
{
	if(inputStream && outputStream)
		[self close];
	NSString *urlString = [NSString stringWithFormat:@"http://%@", ipAddress];
	NSURL *website = [NSURL URLWithString:urlString];
	if (!website) {
		NSLog(@"%@ is not a valid URL", website);
	}
	CFReadStreamRef readStream;
	CFWriteStreamRef writeStream;
	CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)[website host], [portNo intValue], &readStream, &writeStream);
	CFReadStreamSetProperty(readStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
	CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
	inputStream = (NSInputStream *)readStream;
	outputStream = (NSOutputStream *)writeStream;
	[self open];
}  

- (void)open
{
	[inputStream setDelegate:self];
	[outputStream setDelegate:self];
	[inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[inputStream open];
	[outputStream open];
}



-(void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent
{
	NSString *event;    
		
	switch (streamEvent)
	{
	case NSStreamEventNone:
		event = @"NSStreamEventNone";
		break;
	case NSStreamEventOpenCompleted:
		event = @"NSStreamEventOpenCompleted";
		break;
	case NSStreamEventHasBytesAvailable:
		event = @"NSStreamEventHasBytesAvailable";
		if (theStream == inputStream)
		{
			uint8_t buffer[1024];
			int len;
			while ([inputStream hasBytesAvailable])
			{
				len = [inputStream read:buffer maxLength:1024];
				if (len > 0)
				{
					NSMutableString *output = [[NSMutableString alloc] initWithBytes:buffer length:len encoding:NSUTF8StringEncoding];
					NSLog(@"Received data--------------------%@", output);
				}
			}
		}
		break;
	case NSStreamEventHasSpaceAvailable:
		event = @"NSStreamEventHasSpaceAvailable";
		break;
	case NSStreamEventErrorOccurred:
		event = @"NSStreamEventErrorOccurred";             
					
		[self close];
		break;
	case NSStreamEventEndEncountered:
		event = @"NSStreamEventEndEncountered";            
		self close];
		break;
	default:
		vent = @"Unknown";
		break;
	}
    
	NSLog(@"event------%@",event);
}



- (void)close
{
	[inputStream close];
	[outputStream close];
	[inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
	[inputStream setDelegate:nil];
	[outputStream setDelegate:nil];
	inputStream = nil;
	outputStream = nil;            
}


- (void)dataSending:(NSString*)data
{
	if(outputStream)
	{
		if(![outputStream hasSpaceAvailable])
			return;
			
		NSData *_data=[data dataUsingEncoding:NSUTF8StringEncoding];
		int data_len = [_data length];
		uint8_t *readBytes = (uint8_t *)[_data bytes];
		int byteIndex=0;
		unsigned int len=0;
		
		while (TRUE)
		{
			len = ((data_len - byteIndex >= 40960) ?
			  40960 : (data_len-byteIndex));
			
			if(len==0)
				break;
			
			uint8_t buf[len];
			(void)memcpy(buf, readBytes, len);
			len = [outputStream write:(const uint8_t *)buf maxLength:len];
			byteIndex += len;
			readBytes += len;
		}        
		NSLog(@"Sent data----------------------%@",data);               
	}
}