// Client socket
Socket socket = new Socket("78.46.84.171", 80);
Socket socket = new Socket("jenkov.com", 80);

Socket socket = new Socket("jenkov.com", 80);
OutputStream out = socket.getOutputStream();

out.write("some data".getBytes());
out.flush();
out.close();

socket.close();


Socket socket = new Socket("jenkov.com", 80);
InputStream in = socket.getInputStream();

int data = in.read();
//... read more data...

in.close();
socket.close();


// Server socket
ServerSocket serverSocket = new ServerSocket(9000);

ServerSocket serverSocket = new ServerSocket(9000);
boolean isStopped = false;
while(!isStopped){
    Socket clientSocket = serverSocket.accept();

    //do something with clientSocket
}



// InetAddress
InetAddress address = InetAddress.getByName("jenkov.com");
InetAddress address = InetAddress.getByName("78.46.84.171");
InetAddress address = InetAddress.getLocalHost();



// Channel en selectors
SocketChannel socketChannel = SocketChannel.open();
socketChannel.connect(new InetSocketAddress("http://jenkov.com", 80));


ByteBuffer buf = ByteBuffer.allocate(48);
int bytesRead = socketChannel.read(buf);


String newData = "New String to write to file..." + System.currentTimeMillis();
ByteBuffer buf = ByteBuffer.allocate(48);
buf.clear();
buf.put(newData.getBytes());

buf.flip();

while(buf.hasRemaining()) {
    channel.write(buf);
}


socketChannel.close();



// Basic Channel example
RandomAccessFile aFile = new RandomAccessFile("data/nio-data.txt", "rw");
FileChannel inChannel = aFile.getChannel();

ByteBuffer buf = ByteBuffer.allocate(48);

int bytesRead = inChannel.read(buf);
while (bytesRead != -1) {

  System.out.println("Read " + bytesRead);
  buf.flip();

  while(buf.hasRemaining()){
	  System.out.print((char) buf.get());
  }

  buf.clear();
  bytesRead = inChannel.read(buf);
}
aFile.close();